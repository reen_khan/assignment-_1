## Employee_Search_App

*How to run this project?*

1. Install Visual Studio 2019
2. Open as10_EmployeeSearchApp > as10_EmployeeSearchApp.sln in visual studio.
3. Click on the *Start* button from the tool bar on the top to run the project.

*How to use this project?*

1. click on the textbox control in the Window
2. Feed the name of the employee to be searched.
3. The name of the employee with corresponding characters will be displayed.
4. Along with the name, the designation and the pay of the employee is also displayed in the window.

*License*
The GNU General Public License Version 3, is used for it's free software license that ensures users the freedom to analyse ,share and run the software.