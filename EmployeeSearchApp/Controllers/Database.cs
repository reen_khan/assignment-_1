﻿using System.Data.SqlClient;

namespace EmployeeSearchApp.Controllers
{
    public class Database
    {
        #region Singleton
        private static Database _instance;
        public static Database GetDb() => _instance ?? (_instance = new Database());
        #endregion

        private const string DATABASE_NAME = "Personnel";
        private const string CONNECTION_STRING = @"Server=.\SQLEXPRESS;Database=" + DATABASE_NAME + ";Trusted_Connection=True;";
        private const string INIT_CONNECTION_STRING = @"Server=.\SQLEXPRESS;Trusted_Connection=True;";

        #region Queries
        private const string CREATE_DATABASE =
            "IF NOT EXISTS (SELECT \"name\" FROM sysdatabases WHERE \"name\" = '" + DATABASE_NAME + "') " +
            "CREATE DATABASE " + DATABASE_NAME + ";";

        /// <summary>
        /// if table exists
        ///     return CAST(1 as BIT) which means True
        /// else
        ///     return CAST(0 as BIT) which means False
        /// </summary>
        private const string CHECK_TABLE_EXISTS =
            "SELECT IIF(EXISTS(SELECT NAME FROM sysobjects WHERE name = @TableName), CAST(1 AS BIT), CAST(0 AS BIT))";
        #endregion

        public SqlConnection Connection { get; }

        private Database()
        {
            InitDb();

            Connection = new SqlConnection(CONNECTION_STRING);
            Connection.Open();
        }

        private void InitDb()
        {
            using (var conn = new SqlConnection(INIT_CONNECTION_STRING))
            {
                using (var cmd = new SqlCommand(CREATE_DATABASE, conn))
                {
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public bool TableExists(string tableName)
        {
            var cmd = new SqlCommand(CHECK_TABLE_EXISTS, Connection);
            cmd.Parameters.AddWithValue("@TableName", tableName);

            using (cmd)
                return bool.Parse(cmd.ExecuteScalar().ToString());
        }
    }
}