﻿using System.Windows;

namespace EmployeeSearchApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private ViewModel _vm = new ViewModel();

        public MainWindow()
        {
            InitializeComponent();

            DataContext = _vm;
        }
    }
}
