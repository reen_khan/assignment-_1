﻿using EmployeeSearchApp.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace EmployeeSearchApp.Utils
{
    /// <summary>
    /// This string extension means that we can call our method CapitalizeEachWord as a string method
    /// Example:
    ///     "myString".CapitalizeEachWord
    /// </summary>
    public static class StringExtensions
    {
        /// <summary>
        /// Capitalize each word in the source string
        /// Words can be separated not only by space but also by any other separator
        ///
        /// Algorithm:
        /// 1. Split -> Split the source string by the passed separator. It will create an array of strings
        /// 2. Select -> For each string in this array make the first letter capital
        /// 3. Join -> Concatenate all the strings back with the passed separator
        ///
        /// Example 1:
        /// 1. "firstname lastname" -> ["firstname", "lastname"]
        /// 2. ["firstname", "lastname"] -> ["Firstname", "Lastname"]
        /// 3. ["Firstname", "Lastname"] -> "Firstname Lastname"
        ///
        /// Example 2:
        /// 1. "first-name lastname" -> ["first", "name lastname"]
        /// 2. ["first", "name lastname"] -> ["First", "Name lastname"]
        /// 3. ["First", "Name lastname"] -> "First-Name lastname"
        /// </summary>
        public static string CapitalizeEachWord(this string source, char sep = ' ') =>
            string.Join(sep.ToString(), source.Split(sep).Select(n => n.First().ToString().ToUpper() + n.Substring(1)));
    }
    public class EmployeeReader
    {
        private const string DATA_FILE = @"Resources\EmployeeData.csv";
        private static readonly string DEFAULT_PATH = Path.Combine(Environment.CurrentDirectory, DATA_FILE);

        private const char CSV_SEPARATOR = ',';

        public static List<Employee> ReadEmployees()
        {
            var result = new List<Employee>();

            using (var r = new StreamReader(DEFAULT_PATH))
            {
                while (!r.EndOfStream)
                {
                    var data = r.ReadLine().Split(CSV_SEPARATOR).Select(d => d.Trim()).ToArray();
                    // All names in the config file are written with capital letters
                    // So, we need to make it in lower-case and then capitalize each word
                    // Also, some names look like 'Dashed-Name', so we need to capitalize by dashes as well
                    // The same thing about brackets. Some names have a middle name in brackets 'Firstname (Middlename) Lastname'
                    data[0] = data[0].ToLower().CapitalizeEachWord().CapitalizeEachWord('-').CapitalizeEachWord('(');

                    result.Add(new Employee { Name = data[0], Position = data[1], PayRate = decimal.Parse(data[2]) });
                }
            }

            return result;
        }
    }
}